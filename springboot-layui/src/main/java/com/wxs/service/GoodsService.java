package com.wxs.service;

import com.wxs.entity.Goods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wxs
 * @since 2021-01-13
 */
public interface GoodsService extends IService<Goods> {

}
