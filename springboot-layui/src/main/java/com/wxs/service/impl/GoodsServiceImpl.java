package com.wxs.service.impl;

import com.wxs.entity.Goods;
import com.wxs.mapper.GoodsMapper;
import com.wxs.service.GoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wxs
 * @since 2021-01-13
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {

}
