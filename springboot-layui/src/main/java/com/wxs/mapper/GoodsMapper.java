package com.wxs.mapper;

import com.wxs.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wxs
 * @since 2021-01-13
 */
public interface GoodsMapper extends BaseMapper<Goods> {

}
