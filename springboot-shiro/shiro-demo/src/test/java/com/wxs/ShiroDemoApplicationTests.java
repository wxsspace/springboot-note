package com.wxs;

import com.wxs.pojo.User;
import com.wxs.service.UserService;
import com.wxs.util.PasswordGenerateUtil;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ShiroDemoApplicationTests {

    @Autowired
    UserService userService;

    @Test
    void contextLoads() {

        List<User> users = userService.queryUserList();

        users.forEach(System.out::println);

        String pwd = PasswordGenerateUtil.getPassword("root","123456","root",3);
        System.out.println(pwd);
    }

    @Test
    void testMD5(){
        User root = userService.queryUserByName("root");
        System.out.println(root);
        ByteSource bytes = ByteSource.Util.bytes(root.getSalt());
        SimpleHash md5 = new SimpleHash("MD5", "123456",bytes, 3);
        System.out.println(md5);

        String pwd = PasswordGenerateUtil.getPassword("root","123456","root",3);
        System.out.println(pwd);
    }



}
