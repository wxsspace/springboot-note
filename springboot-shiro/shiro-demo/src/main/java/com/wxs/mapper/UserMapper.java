package com.wxs.mapper;

import com.wxs.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@Mapper
public interface UserMapper {

    // 获取所有人信息
    List<User> queryUserList();

    User queryUserByName(@Param("name") String username);

    // 通过id获得信息
    User queryUserById(@Param("id") int id);

    int addUser(User user);

    int updateUser(User user);

    int deleteUser(int id);
}
