package com.wxs.service;

import com.wxs.pojo.User;

import java.util.List;

public interface UserService {

    public List<User> queryUserList();

    public User queryUserByName(String name);
}
