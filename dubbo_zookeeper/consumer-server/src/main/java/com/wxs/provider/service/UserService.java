package com.wxs.provider.service;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Component;

@Component
public class UserService {
    //我们需要去拿去注册中心的服务
    @DubboReference
    TicketService ticketService;

    public void bugTicket() {

        String ticket = ticketService.getTicket();

        System.out.println("在注册中心买到" + ticket);
    }

}
