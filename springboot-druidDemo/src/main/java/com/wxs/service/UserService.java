package com.wxs.service;

import com.wxs.mapper.UserMapper;
import com.wxs.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    // 获取所有人信息
    public List<User> queryUserList(){
        return userMapper.queryUserList();
    }

    // 通过id获得信息
    public User queryUserById(int id){
        return userMapper.queryUserById(id);
    }

    public int addUser(User user){
        return userMapper.addUser(user);
    }

    public int updateUser(User user){
        return userMapper.updateUser(user);
    }

    public int deleteUser(int id){
        return userMapper.deleteUser(id);
    }

}
