package com.wxs.controller;


import com.wxs.pojo.User;
import com.wxs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class DruidController {

    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public List<User> queryUserList(){
        return userService.queryUserList();
    }
    @GetMapping("/query/{id}")
    public User queryUserById(@PathVariable("id") Integer id){
        return userService.queryUserById(id);
    }
    @GetMapping("/add")
    public int addUser(User user){
        return userService.addUser(user);
    }
    @GetMapping("/update")
    public int updateUser(User user){
        return userService.updateUser(user);
    }
    @GetMapping("/del/{id}")
    public int delUser(@PathVariable("id")Integer id){
        return delUser(id);
    }



}
