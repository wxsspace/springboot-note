package com.wxs.controller;


import com.wxs.dao.DepartmentDao;
import com.wxs.dao.EmployeeDao;
import com.wxs.pojo.Department;
import com.wxs.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.PublicKey;
import java.util.Collection;

@Controller
public class EmployeeController {

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private EmployeeDao employeeDao;

    @RequestMapping("/emps")
    public String getAllEmploee(Model model){
        Collection<Employee> emplees = employeeDao.getEmplees();
        model.addAttribute("emps",emplees);
        return "emp/list";
    }

    @GetMapping("/emp")
    public String toAdd(Model model){

        Collection<Department> departments = departmentDao.getDepartments();
        model.addAttribute("departments",departments);
        return "emp/add";
    }

    @PostMapping("/emp")
    public String addEmp(Employee employee){
        System.out.println(employee);

        employeeDao.save(employee); /*保存员工信息*/
        return "redirect:/emps";
    }

    /*去员工的修改页面*/
    @GetMapping("/emp/{id}")
    public String toUpdate(@PathVariable("id") int id , Model model){
        Employee employee = employeeDao.getEmployeeById(id);
        model.addAttribute("employee",employee);
        Collection<Department> departments = departmentDao.getDepartments();
        model.addAttribute("departments",departments);
        return "emp/update";
    }
    /*修改页面*/
    @PostMapping("/updateEmp")
    public String updateEmp(Employee employee) {
        employeeDao.save(employee); /*保存员工信息*/
        return "redirect:/emps";
    }

    /*删除*/
    @GetMapping("/deleEmp/{id}")
    public String delEmp(@PathVariable("id") Integer id){
        employeeDao.deleEmp(id);
        return "redirect:/emps";
    }

}
