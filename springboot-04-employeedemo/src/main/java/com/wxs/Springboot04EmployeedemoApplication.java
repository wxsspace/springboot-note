package com.wxs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot04EmployeedemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot04EmployeedemoApplication.class, args);
    }

}
