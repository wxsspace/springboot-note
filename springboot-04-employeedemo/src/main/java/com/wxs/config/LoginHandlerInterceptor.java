package com.wxs.config;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        /*登录成功之后应该偶用户的session*/
        Object loginUser = request.getSession().getAttribute("loginUser");
        if(loginUser == null){
            request.setAttribute("error","没有权限, 请先登录");
            request.getRequestDispatcher("/index.html").forward(request,response);
            return false; /*不放行*/
        }else{
            return true; /*放行*/
        }
    }
}
