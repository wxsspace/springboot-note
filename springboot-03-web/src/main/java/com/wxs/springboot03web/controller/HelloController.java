package com.wxs.springboot03web.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String gethello(){
        return "hello web 开发！";
    }
}
