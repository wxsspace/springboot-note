package com.wxs.springboot03web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;

@Controller
public class TestController {

    @RequestMapping("/test")
    public String test(Model model){

        model.addAttribute("msg","Hello Thymeleaf!");
        model.addAttribute("umsg","<h1>原生HTML</h1>");
        model.addAttribute("users", Arrays.asList("wxs","xyl"));

        return "test";
    }


}
