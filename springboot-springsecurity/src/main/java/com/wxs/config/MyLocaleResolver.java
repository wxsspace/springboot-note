package com.wxs.config;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

public class MyLocaleResolver implements LocaleResolver {

    /*解析请求*/
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        /*获取请求参数*/
        String lang = request.getParameter("l");
        Locale locale = Locale.getDefault();/*如果没有改参数直接使用默认的 解析器*/
        if(!StringUtils.isEmpty(lang)){
            String[]  split = lang.split("_");
            /*语言 + 国家*/
            locale = new Locale(split[0],split[1]);
        }

        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

    }
}
