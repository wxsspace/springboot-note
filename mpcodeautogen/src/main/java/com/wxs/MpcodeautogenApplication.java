package com.wxs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.wxs..blog.mapper")
@SpringBootApplication
public class MpcodeautogenApplication {

    public static void main(String[] args) {
        SpringApplication.run(MpcodeautogenApplication.class, args);
    }

}
