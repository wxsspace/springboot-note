package com.wxs.blog.service;

import com.wxs.blog.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wxs
 * @since 2021-01-05
 */
public interface UserService extends IService<User> {

}
