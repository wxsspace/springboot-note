package com.wxs.blog.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wxs
 * @since 2021-01-05
 */
@RestController
@RequestMapping("/blog/user")
public class UserController {

}

