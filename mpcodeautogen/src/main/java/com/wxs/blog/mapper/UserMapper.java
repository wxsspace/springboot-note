package com.wxs.blog.mapper;

import com.wxs.blog.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wxs
 * @since 2021-01-05
 */
public interface UserMapper extends BaseMapper<User> {

}
