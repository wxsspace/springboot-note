package com.wxs;

import com.wxs.pojo.Article;
import com.wxs.service.ArticleService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SpringeditApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    ArticleService articleService;

    @Test
    void test(){
        List<Article> articles = articleService.queryArticles();
        articles.forEach(System.out::println);
    }
}
