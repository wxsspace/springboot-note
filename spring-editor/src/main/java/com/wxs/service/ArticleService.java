package com.wxs.service;

import com.wxs.mapper.ArticleMapper;
import com.wxs.pojo.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleService implements ArticleMapper {

    @Autowired
    ArticleMapper articleMapper;

    @Override
    public List<Article> queryArticles() {
        return articleMapper.queryArticles();
    }

    @Override
    public int addArticle(Article article) {
        return articleMapper.addArticle(article);
    }

    @Override
    public Article getArticleById(int id) {
        return articleMapper.getArticleById(id);
    }

    @Override
    public int deleteArticleById(int id) {
        return articleMapper.deleteArticleById(id);
    }
}
