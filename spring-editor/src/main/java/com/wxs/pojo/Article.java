package com.wxs.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Article implements Serializable {

    private Integer id;
    private String author;
    private String title;
    private String content;
}
