package com.wxs.mapper;

import com.wxs.pojo.Article;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ArticleMapper {
    List<Article> queryArticles();

    int addArticle(Article article);

    Article getArticleById(int id);

    int deleteArticleById(int id);
}
