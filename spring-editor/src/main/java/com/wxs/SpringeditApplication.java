package com.wxs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringeditApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringeditApplication.class, args);
    }

}
