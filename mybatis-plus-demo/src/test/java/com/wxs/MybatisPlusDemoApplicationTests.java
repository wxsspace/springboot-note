package com.wxs;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wxs.mapper.UserMapper;
import com.wxs.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
class MybatisPlusDemoApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    void contextLoads() {
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }


    @Test
    void insertTest(){
        User user = new User();
        user.setName("wxs-test");
        user.setAge(3);
        user.setEmail("123456@qq.com");

        int res = userMapper.insert(user);
        System.out.println(res);
        System.out.println(user);
    }

    /*update optation*/
    @Test
    void updateuser(){
        User user = new User();
        user.setId(6L);
        user.setName("wxshen");
        user.setAge(18);

        int i = userMapper.updateById(user);
        System.out.println(i);
    }

    @Test
    /*单线程执行乐观锁成功*/
    public void testOptimistionClocker(){
        User user = userMapper.selectById(1L);
        System.out.println(user);
        /*修改用户信息*/
        user.setName("小沈");
        user.setEmail("123890@qq.com");

        userMapper.updateById(user);
    }

    /*多线程 乐观锁执行失败*/
    @Test
    public void testOptimistion2(){
        /*thread 1*/
        User user = userMapper.selectById(1L);
        user.setName("哈哈");

        /*thread 2
        * 模拟另外一个线程执行了插队操作
        * */
        User user1 = userMapper.selectById(1L);
        user1.setName("sbjfasfasf");
        userMapper.updateById(user1);

        userMapper.updateById(user); /*自旋锁来多次尝试提交！
                                        如果没有乐观锁就会覆盖插队线程的值！*/
    }


    /*测试查询*/
    @Test
    public void testSelectbyId(){
        User user = userMapper.selectById(1L);
        System.out.println(user);
    }

    /*测试批量查询*/
    @Test
    public void testSelectbyBatch(){
        List<User> users = userMapper.selectBatchIds(Arrays.asList(1, 2, 3, 4));
        users.forEach(System.out::println);
    }

    /*测试条件查询*/
    @Test
    public void testSelectbyBatchIDs(){
        HashMap<String,Object> map = new HashMap<>();

        /*自定义查询的元素*/
        map.put("name","wxshen");
        map.put("age",18);

        List<User> users = userMapper.selectByMap(map);
        users.forEach(System.out::println);
    }


    /*分页查询*/
    @Test
    public void testpage(){
        Page<User> page = new Page<>(2,3);
        userMapper.selectPage(page,null);

        List<User> users = page.getRecords();
        users.forEach(System.out::println);
        System.out.println(page.getTotal());
    }

    @Test
    public void testdelete(){
        userMapper.deleteById(1345927863045914627L);
    }

    /*测试逻辑删除*/
    @Test public void testDeleteLogic(){
        userMapper.deleteById(1L);
    }

    /*条件查询*/
    @Test
    public void test1(){
        /*查询名字不为空的用户*/
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.isNotNull("name")
                .isNotNull("email")
                .ge("age",10);
        userMapper.selectList(wrapper).forEach(System.out::println);
    }




}
