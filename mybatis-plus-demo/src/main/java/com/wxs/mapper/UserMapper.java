package com.wxs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wxs.pojo.User;
import org.springframework.stereotype.Repository;

/*在mapper上集成baseMapper*/
@Repository /*持久层*/
public interface UserMapper extends BaseMapper<User> {
    /*所有的CRUD 操作已经完成*/
    /*需要注意的是我们需要再主启动类上面住得扫描mapper包下面的所有接口*/
}
